(* The Grammar file *)

structure Grammar =
struct

  (*
  * type Symbol = Atom.atom
  * type Token  = Atom.atom
  *)

  (* 
  * AtomSet represents sets of Atoms
  * AtomMap represent maps (aka dictionaries) using Atoms
  * Sets and Maps are defined based on an ordering structure
  * Here, we have a set structure for representing the RHS of rules
  *  For that, we define structure with signature ORD_KEY for RHS
  * *)

  type Symbol = AtomSet.set
  type Token  = AtomSet.set

  type RHS     = Atom.atom list 

  structure RHS_KEY : ORD_KEY = struct
    type ord_key = RHS
    val compare  = List.collate Atom.compare
  end

  (* Now, we create a set of RHS's
  * *)
  structure RHSSet = RedBlackSetFn (RHS_KEY)

  type Productions = RHSSet.set

  (* Finally, rules of a grammar form a Map/Dictionary
  *   with keys   - symbols
  *    and values - Production Rules with the symbol
  * *)

  type Rules       = Productions AtomMap.map

  type Grammar     = { symbols : Symbol, tokens : Token, rules : Rules }

end
