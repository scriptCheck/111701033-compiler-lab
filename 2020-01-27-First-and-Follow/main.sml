use "grammer.sml";
use "input.sml";

open Grammar;

val nullable : Atom.atom list ref = ref nil
val first : (AtomSet.set ref) AtomMap.map ref = ref AtomMap.empty
val follow : (AtomSet.set ref) AtomMap.map ref = ref AtomMap.empty

fun insert_symbol mapp (x::xs) = ( mapp := AtomMap.insert 
                                    (!mapp, x, ref AtomSet.empty);
                                   insert_symbol mapp xs)
  | insert_symbol _     _     = ()

fun initialise mapp = insert_symbol 
                           mapp 
                           (AtomSet.listItems (#symbols grammar));

initialise first;
initialise follow;

(* helper functions *)
fun isMemberAtomlist li x = let
  fun comp y = ( Atom.compare (x,y) = EQUAL )
in
  List.exists comp li
end

fun printAtomlist (x::xs::xss) = (print ((Atom.toString x)^", ");
                                  printAtomlist (xs::xss))
  | printAtomlist (x::xs)      = (print ((Atom.toString x));
                                  printAtomlist xs)
  | printAtomlist _            = (print " }\n")

fun printAtomSet x = printAtomlist (AtomSet.listItems x)

fun printAtomAtomsetList str ((x::xs) : (Atom.atom * (AtomSet.set ref)) list) =
  (print ((Atom.toString (#1 x)) ^ str);
   printAtomSet (!(#2 x));
   printAtomAtomsetList str xs)
  | printAtomAtomsetList _      _ = ()

fun printAtomAtomsetMap y = printAtomAtomsetList (" :- { ") (AtomMap.listItemsi y)

fun printAtomAtomsetMapFollow y = printAtomAtomsetList (" :- { $, ") (AtomMap.listItemsi y)

(* used for stopping fixed point recursion *)
val cont = ref false

(* nullable *)
fun isNullableSymbol x = if (Atom.compare (x, Atom.atom "EPS") = EQUAL)
                         then true
                         else if AtomSet.member (#tokens grammar, x)
                              then false
                              else isMemberAtomlist (!nullable) x

fun isNullableProduction (x::xs) = if not (isNullableSymbol x)
                                   then false
                                   else isNullableProduction xs
  | isNullableProduction _       = true

fun checkNullableRule (x::xs) = if isNullableProduction x 
                                then true
                                else checkNullableRule xs
  | checkNullableRule _       = false

fun checkNullableSymbol x = let
  val rhsList = RHSSet.listItems (AtomMap.lookup (#rules grammar, x))
in
  checkNullableRule rhsList
end

fun checkNullableSymbols (x::xs) = (if isMemberAtomlist (!nullable) x
                                    then ()
                                    else if checkNullableSymbol x
                                         then (nullable := x :: !nullable;
                                               cont := true)
                                         else ();
                                    checkNullableSymbols xs)
  | checkNullableSymbols _       = ()

fun findNullable () = (
  cont := false;
  checkNullableSymbols (AtomSet.listItems (#symbols grammar));
  if (!cont)
  then findNullable ()
  else ()
  )

(* first *)
fun addFirstSymbol y x = let 
  val fstX = if AtomSet.member (#tokens grammar, x)
               then AtomSet.add (AtomSet.empty, x)
               else if Atom.same (x, Atom.atom "EPS")
                    then AtomSet.empty
                    else !(AtomMap.lookup (!first, x))
  val fstY = !(AtomMap.lookup (!first, y))
in
  if AtomSet.isSubset (fstX, fstY)
  then ()
  else (cont := true;
        AtomMap.lookup (!first, y) := AtomSet.union (fstX, fstY))
end

fun findFirstProd y (x::xs) = (addFirstSymbol y x;
                               if isMemberAtomlist (!nullable) x
                               then findFirstProd y xs
                               else ())
  | findFirstProd _      _  = ()

fun findFirstRule y (x::xs) = (findFirstProd y x;
                               findFirstRule y xs)
  | findFirstRule _      _  = ()

fun findFirstSymbol x = let
  val rhsList = RHSSet.listItems (AtomMap.lookup (#rules grammar, x))
in
  findFirstRule x rhsList
end

fun findFirstSymbols (x::xs) = (findFirstSymbol x;
                                findFirstSymbols xs)
  | findFirstSymbols      _  = ()

fun findFirst () = (
  cont := false;
  findFirstSymbols (AtomSet.listItems (#symbols grammar));
  if (!cont)
  then findFirst ()
  else
    ()
    )

(* follow *)
fun addFollowSymbol y x (xs::xss) = (
  let 
    val fstXS = if AtomSet.member (#tokens grammar, xs)
                then AtomSet.add (AtomSet.empty, xs)
                else !(AtomMap.lookup (!first, xs))
    val follX = !(AtomMap.lookup (!follow, x))
  in
    if AtomSet.isSubset (fstXS, follX)
    then ()
    else (cont := true;
          AtomMap.lookup (!follow, x) := AtomSet.union (fstXS, follX))
  end;
  if isMemberAtomlist (!nullable) xs
  then addFollowSymbol y x xss
  else ()
  )
  | addFollowSymbol y x       _  = let
       val follY = !(AtomMap.lookup (!follow, y))
       val follX = !(AtomMap.lookup (!follow, x))
     in
       if AtomSet.isSubset (follY, follX)
       then ()
       else (cont := true;
             AtomMap.lookup (!follow, x) := AtomSet.union (follX, follY))
     end

fun findFollowProd y (x::xs) = (if AtomSet.member (#symbols grammar, x)
                                then addFollowSymbol y x xs
                                else ();
                                findFollowProd y xs)
  | findFollowProd _      _  = ()

fun findFollowRule y (x::xs) = (findFollowProd y x;
                                findFollowRule y xs)
  | findFollowRule _      _  = ();

fun findFollowSymbol x = let
  val rhsList = RHSSet.listItems (AtomMap.lookup (#rules grammar, x))
in
  findFollowRule x rhsList
end

fun findFollowSymbols (x::xs) = (findFollowSymbol x;
                                 findFollowSymbols xs)
  | findFollowSymbols      _  = ();

fun findFollow () = (
  cont := false;
  findFollowSymbols (AtomSet.listItems (#symbols grammar));
  if (!cont)
  then findFollow ()
  else ()
  );

(* test on example *)
findNullable ();
findFirst ();
findFollow ();
(print ("\n Nullable symbols: { "); printAtomlist (!nullable));
(print ("\n First for each token: \n"); printAtomAtomsetMap (!first));
(print ("\n Follow for tokens: \n"); printAtomAtomsetMapFollow (!follow))
