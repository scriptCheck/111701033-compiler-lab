(* Input File with sample grammar *)

use "grammer.sml";
open Grammar

(*   E   ->  TE'
*    E'  -> +TE' | eps
*    T   ->  FT'
*    T'  -> *FT' | eps
*    F   ->  id  | (E)
* *)

(* Symbols aka Non Terminals *)
val symbols  = ref AtomSet.empty;
    symbols := AtomSet.addList (!symbols, [Atom.atom "E",
                                           Atom.atom "T",
                                           Atom.atom "E'",
                                           Atom.atom "T'",
                                           Atom.atom "F"
                                           ]);

(* Tokens aka Terminals *)
val tokens  = ref AtomSet.empty;
    tokens := AtomSet.addList (!tokens, [Atom.atom "+",
                                        Atom.atom "*",
                                        Atom.atom "(",
                                        Atom.atom ")",
                                        Atom.atom "$",
                                        Atom.atom "id"
                                        ]);

(* production *)
val PE  = ref RHSSet.empty;
    PE := RHSSet.add (!PE, [Atom.atom "T", Atom.atom "E'"]);

val PED  = ref RHSSet.empty;
    PED := RHSSet.add (!PED, [Atom.atom "+",
                              Atom.atom "T", Atom.atom "E'"]);
    PED := RHSSet.add (!PED, [Atom.atom "EPS"]);

val PT  = ref RHSSet.empty;
    PT := RHSSet.add (!PT, [Atom.atom "F", Atom.atom "T'"]);

val PTD  = ref RHSSet.empty;
    PTD := RHSSet.add (!PTD, [Atom.atom "*",
                              Atom.atom "F", Atom.atom "T'"]);
    PTD := RHSSet.add (!PTD, [Atom.atom "EPS"]);

val PF  = ref RHSSet.empty;
    PF := RHSSet.add (!PF, [Atom.atom "(", Atom.atom "E", Atom.atom ")"]);
    PF := RHSSet.add (!PF, [Atom.atom "id"]);

(* Production Rules *)
val rules : Rules ref = ref AtomMap.empty;
    rules := AtomMap.insert (!rules, Atom.atom "E", !PE);
    rules := AtomMap.insert (!rules, Atom.atom "E'", !PED);
    rules := AtomMap.insert (!rules, Atom.atom "T", !PT);
    rules := AtomMap.insert (!rules, Atom.atom "T'", !PTD);
    rules := AtomMap.insert (!rules, Atom.atom "F", !PF);

(* Finally, Grammar *)
val grammar : Grammar = { symbols = !symbols,
                          tokens  = !tokens,
                          rules   = !rules };
