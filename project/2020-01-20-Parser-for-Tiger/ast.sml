structure Ast = struct

datatype Expr  = Nil
               | ID     of string
               | Str    of string
               | Const  of int
               | Op     of Expr * BinOp * Expr
               | Cp     of Expr * BinCp * Expr
               | Seq    of Expr list
               | If     of Expr * Expr * Expr option
               | While  of Expr * Expr
               | For    of Expr * Expr * Expr * Expr
               | Break
               | Let    of Dec list * Expr list 
               | FnCall of string * Expr list
               | LVal   of Var
               | Assign of Var * Expr
               | Array  of string * Expr * Expr
               | Record of string * (string * Expr) list
               | Object of string
               | MethodCall of Var * string * Expr list

     and Var   = SimpleVar    of string
               | DotVar       of Var * string
               | ExprVar      of Var * Expr

     and BinOp = Plus
               | Minus
               | Mul
               | Div

     and BinCp = GreaterEq   (* >= *)
               | LessEq      (* <= *)
               | Eq          (*  = *)
               | NotEq       (* <> *)
               | LessThan    (*  < *)
               | GreaterThan (*  > *)
     
     and Dec   = VarDec of string * string option * Expr
               | TypeDec of string * Ty
               | FunctionDec of funDec
     
     and Ty    = NameTy of string
               | RecordTy of field list
               | ArrayTy of string

     withtype field  = string * string
          and funDec = string * (string * string) list * string option * Expr
(*
* | AndOP       (*  & *)
  | OrOP        (*  | *)
  *)

datatype Program = Exptype of Expr
                 | Dectype of Dec list

fun binOpDenote Plus        x y = x + y
  | binOpDenote Minus       x y = x - y
  | binOpDenote Mul         x y = x * y
  | binOpDenote Div         x y = x div y
  
fun binCpDenote GreaterEq   x y = x >= y
  | binCpDenote LessEq      x y = x <= y
  | binCpDenote Eq          x y = x = y
  | binCpDenote NotEq       x y = x <> y
  | binCpDenote LessThan    x y = x < y
  | binCpDenote GreaterThan x y = x > y
(*
* | binCpDenote AndOP       x y = x andalso y
* | binCpDenote OrOP        x y = x  orelse y
* *)

(*
* fun exprDenote Nil             = 
* | exprDenote (Const x)       = x
* | exprDenote (Op (x,oper,y)) = binOpDenote oper (exprDenote x) (exprDenote y);
*)

(* Conversion to strings *)

fun binOpToString Plus        =  "+"
  | binOpToString Minus       =  "-"
  | binOpToString Mul         =  "*"
  | binOpToString Div         =  "/"

fun binCpToString GreaterEq   = ">="
  | binCpToString LessEq      = "<="
  | binCpToString Eq          =  "="
  | binCpToString NotEq       = "<>"
  | binCpToString LessThan    =  "<"
  | binCpToString GreaterThan =  ">"
(*
* | binCpToString AndOP       =  "&"
* | binCpToString OrOP        =  "|"
*)

(* Some helper functions *)
fun plus   a b = Op (a,        Plus, b)
fun minus  a b = Op (a,       Minus, b)
fun mul    a b = Op (a,         Mul, b)
fun divide a b = Op (a,         Div, b)

fun grEq   a b = Cp (a,   GreaterEq, b)
fun leEq   a b = Cp (a,      LessEq, b)
fun eq     a b = Cp (a,          Eq, b)
fun notEq  a b = Cp (a,       NotEq, b)
fun less   a b = Cp (a,    LessThan, b)
fun great  a b = Cp (a, GreaterThan, b)
(*
* fun andOp  a b = Cp (a,       AndOP, b)
* fun orOp   a b = Cp (a,        OrOP, b)
*)

end
