structure TIGC =
struct

(*

1. Use the `ExprLrValsFun` functor to create an `ExprLrVals`
   structure.  The `ExprLrVals` thus created has manythings required
   by the parser like the parsing table. It also has the Tokens data
   type of the language and the lexer needs to know this.

2. The substructure `ExprLrVals.Tokens` is then passed to the lexer
   generator functor `ExprLexFun` to get the lexer structure `ExprLex`

3. Finally, the lexer (available in `ExprLex`) is combined with the
   parsing data to build the parser. Note that the parser needs to
   know the lexing function as it is used by the parser to get tokens.

NOTE: The names of the structures are all controllable. For example
our functors are called ExprLrValsFun and ExprLexFun due to the line
starting with `%name` in expr.grm and `%header` line in the expr.lex
respectively.

*)

structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure TigerLex    = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerParser = Join( structure ParserData = TigerLrVals.ParserData
			      structure Lex        = TigerLex
			      structure LrParser   = LrParser
			   )

(* Build Lexers *)
fun makeTigerLexer strm = TigerParser.makeLexer (fn n => TextIO.inputN(strm,n))
val makeFileLexer      = makeTigerLexer o TextIO.openIn


(* Parse command line and set a suitable lexer *)
val thisLexer = case CommandLine.arguments() of
		    []  => makeTigerLexer TextIO.stdIn
		 |  [x] => makeFileLexer x
		 |  _   => (TextIO.output(TextIO.stdErr, "usage: ./tigc file"); OS.Process.exit OS.Process.failure)

fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr,
					    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")

val parsed = #1 ( TigerParser.parse (0,thisLexer,print_error,()) )
(* 
* val executable  = Translate.compile parsed                      (* compiling/code generation *)
  val _           = TextIO.output(TextIO.stdOut, Machine.programToString executable)
			       (* writing out the executable (in this case rp expression ) *) *)
end
