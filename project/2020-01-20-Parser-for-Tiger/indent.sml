structure INDENT =
struct

  fun put s                      = if (s=0) 
                                   then ("") 
                                   else (" "^(put (s-1)))

  fun indent s (Ast.Nil)           = "nil"
    | indent s (Ast.ID x)          = x
    | indent s (Ast.Str x)         = x
    | indent s (Ast.Const y)       = Int.toString y
    | indent s (Ast.Op (x,oper,y)) = "("^
                                   (indent 0 x)^
                                   (Ast.binOpToString oper)^
                                   (indent 0 y)^")"
    | indent s (Ast.Cp (x,oper,y)) = "("^
                                   (indent 0 x)^
                                   (Ast.binCpToString oper)^
                                   (indent 0 y)^")"
    | indent s (Ast.Seq z)         = "(\n"^
                                   (indentSeq (s+4) z)^
                                   (put s)^")"
    | indent s (Ast.If     x)      = indentIf s x
    | indent s (Ast.While  x)      = indentWhile s x
    | indent s (Ast.For    x)      = indentFor s x
    | indent s (Ast.Break   )      = (put s)^"break\n"
    | indent s (Ast.Let    x)      = indentLet s x
    | indent s (Ast.FnCall x)      = indentFnCall s x
    | indent s (Ast.LVal   x)      = indentLVal s x
    | indent s (Ast.Assign x)      = indentAssign s x
    | indent s (Ast.Array  x)      = indentArray s x
    | indent s (Ast.Record x)      = indentRecord s x
    | indent s (Ast.Object x)      = (put s)^"new "^x
    | indent s (Ast.MethodCall x)  = indentMethodCall s x

  and indentSeq s []               = "\n"
    | indentSeq s (x::[])          = (indent s x)^"\n"
    | indentSeq s (x::xs)          = (indent s x)^";\n"^(indentSeq s xs)

  (*  indentIF (Ast.Expr,Ast.Expr,Ast.Expr option -> string *)
  and indentIf s (x,y,z)           = let 
                                        val i_x  = indent (s+4) x
                                        val i_y  = indent (s+4) y
                                        val i_if = (put s)^"if\n"^i_x^"\n"^
                                                   (put s)^"then\n"^i_y
                                     in 
                                        case z of
                                             (NONE  )  => i_if
                                           | (SOME e)  => i_if^"\n"^
                                                          (put
                                                          s)^"else\n"^(indent (s+4) e)
                                     end
  
  and indentWhile s (x,y)          = let
                                      val i_x = indent 0 x
                                      val i_y = indent (s+4) y
                                   in
                                      (put s)^"while "^i_x^" do"^"\n"^i_y
                                   end
  
  and indentFor s (x,y,z,w)        = let
                                      val i_x = indent 0 x
                                      val i_y = indent 0 y
                                      val i_z = indent 0 z
                                      val i_w = indent (s+4) w
                                   in
                                      (put s)^"for "^i_x^
                                      " := "^i_y^" to "^i_z
                                      ^" do"^"\n"^i_w
                                   end

  and indentLet s (x,y)            = let
                                      val i_x = indentDecs (s+4) x
                                      val i_y = indentSeq (s+4) y
                                   in
                                      (put s)^"let\n"
                                      ^i_x^(put s)^"in\n"
                                      ^i_y^(put s)^"end"
                                   end
  
  and indentFnCall s (x,y)         = let
                                      val indent_y = printArgs y
                                   in
                                      (put s)^x^" ("^indent_y^")"
                                   end
  and printArgs []                 = ""
    | printArgs (x::[])            =  indent 0 x
    | printArgs (x::xs)            = (indent 0 x)^", "^(printArgs xs)
  
  and indentMethodCall s (x,y,z)   = let
                                      val indent_x = indentVar 0 x
                                      val indent_z = printArgs z
                                   in
                                      (put s)^indent_x^"."^y^" ("^indent_z^")"
                                   end

  and indentLVal s x               = indentVar s x

  and indentVar s (Ast.SimpleVar   x) = (put s)^x
    | indentVar s (Ast.DotVar  (x,y)) = (put s)^(indentVar 0 x)^"."^y
    | indentVar s (Ast.ExprVar (x,y)) = (put s)^(indentVar 0 x)^"["^(indent 0 y)^"]"
  
  and indentDecs s []               = ""
    | indentDecs s (x::xs)          = (indentDec s x)^";\n"^(indentDecs s xs)
  
  and indentDec s (Ast.VarDec (x,y,e)) =    let
                                               val indent_e = indent (s+1) e
                                               val indentDP = (put s)^"var "^x
                                            in
                                              case y of SOME z => indentDP^": "^z^" := \n"^(put (s+10+(String.size (x^z))))^indent_e 
				                      | NONE   => indentDP^" := \n"^(put (s+10+(String.size (x))))^indent_e
                                            end
    | indentDec s (Ast.TypeDec (x,y))       = (put s)^"type "^x^"= "^(printTy 0 y)
    | indentDec s (Ast.FunctionDec (x,y,z,w)) = let
                                                  val indent_y     = printField y
                                                  val indent_w     = indent s w
                                                  val indentFunDec = (put s)^"function "^x^"("^(indent_y)^")"
                                                in
                                                  case z of NONE    => indentFunDec^" = "^(indent_w)^" \n "
				                          | SOME e  => indentFunDec^": "^e^" = "^(indent_w)^" \n "
                                                end

  and printTy s (Ast.NameTy   x) = x
    | printTy s (Ast.RecordTy x) = "{"^(printField x)^"}"
    | printTy s (Ast.ArrayTy  x) = "array of "^x

  and printField []           = " "
    | printField ((x,y)::[])  = x^": "^y
    | printField ((x,y)::xys) = x^": "^y^", "^(printField xys) 

  and indentAssign s (x,y)          = let
                                       val indent_x = indentVar 0 x
                                       val indent_y = indent 1 y  
                                    in
                                       (put s)^indent_x^" := \n"^
                                       (put (s+4+(String.size indent_x)))^indent_y
                                    end

  and indentArray s (x,y,z)         = let
                                       val indent_y = indent 0 y
                                       val indent_z = indent 0 z
                                    in
                                       (put s)^x^"["^indent_y^"] of"^indent_z
                                    end

  and indentRecord s (x,y)          = let
                                       fun combine [] = ""
                                         | combine ((z,w)::[]) = z^" = "
                                                                 ^(indent 0 w)
                                         | combine ((z,w)::zws)= z^" = "
                                                                 ^(indent 0 w)^", "
                                                                 ^(combine zws)
                                       val indent_y = combine y
                                    in
                                      (put s)^x^"{"^indent_y^"}"
                                    end

  and indentProgram (Ast.Exptype x) = indent 0 x
    | indentProgram (Ast.Dectype x) = indentDecs 0 x

  val program  = TIGC.parsed    (* getting parsed code *)
  val indented = (indentProgram program)^"\n"
  val _        = TextIO.output(TextIO.stdOut, indented)

end
