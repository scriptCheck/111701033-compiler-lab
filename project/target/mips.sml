structure MIPS = struct

datatype Regs = ZERO
              | V0 | V1
              | A0 | A1 | A2 | A3
              | T0 | T1 | T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9
              | S0 | S1 | S2 | S3 | S4 | S5 | S6 | S7
              | K0 | K1
              | GP
              | SP
              | FP
              | RA

datatype  ('l,'t) Inst = Add     of 't * 't * 't
                       | Sub     of 't * 't * 't
                       | Addi    of 't * 't * int
                       | Addu    of 't * 't * 't
                       | Subu    of 't * 't * 't
                       | Addiu   of 't * 't * int
                       | Mul     of 't * 't * 't
                       | Mult    of 't * 't 
                       | Div     of 't * 't 
                       | And     of 't * 't * 't
                       | Or      of 't * 't * 't
                       | Andi    of 't * 't * int
                       | Ori     of 't * 't * int
                       | Sll     of 't * 't * int
                       | Srl     of 't * 't * int
                       | Load    of 't * int * 't
                       | Store   of 't * int * 't
                       | Lui     of 't * int
                       | La      of 't * 'l
                       | Li      of 't * int
                       | Mfhi    of 't
                       | Mflo    of 't
                       | Move    of 't * 't
                       | Beq     of 't * 't * int
                       | Bne     of 't * 't * int
                       | Bgt     of 't * 't * int
                       | Bge     of 't * 't * int
                       | Blt     of 't * 't * int
                       | Ble     of 't * 't * int
                       | Slt     of 't * 't * 't
                       | Slti    of 't * 't * int
                       | J       of int
                       | Jr      of 't
                       | Jal     of int
                       | Syscall 

(*
   Converts an instruction where the labels are strings and temps are
   actual machine registers to a string. This function is used at the
   end to emit the actual code.

   The pretty function has the type:

   val pretty : (string, regs) inst -> string

*)

fun prettyRegs ZERO = "$zero"
  | prettyRegs V0   = "$v0"
  | prettyRegs V1   = "$v1"
  | prettyRegs A0   = "$a0"
  | prettyRegs A1   = "$a1"
  | prettyRegs A2   = "$a2"
  | prettyRegs A3   = "$a3"
  | prettyRegs T0   = "$t0"
  | prettyRegs T1   = "$t1"
  | prettyRegs T2   = "$t2"
  | prettyRegs T3   = "$t3"
  | prettyRegs T4   = "$t4"
  | prettyRegs T5   = "$t5"
  | prettyRegs T6   = "$t6"
  | prettyRegs T7   = "$t7"
  | prettyRegs T8   = "$t8"
  | prettyRegs T9   = "$t9"
  | prettyRegs S0   = "$s0"
  | prettyRegs S1   = "$s1"
  | prettyRegs S2   = "$s2"
  | prettyRegs S3   = "$s3"
  | prettyRegs S4   = "$s4"
  | prettyRegs S5   = "$s5"
  | prettyRegs S6   = "$s6"
  | prettyRegs S7   = "$s7"
  | prettyRegs K0   = "$k0"
  | prettyRegs K1   = "$k1"
  | prettyRegs GP   = "$gp"
  | prettyRegs SP   = "$sp"
  | prettyRegs FP   = "$fp"
  | prettyRegs RA   = "$ra"

fun pretty (Add     (x,y,z) ) = "add "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Sub     (x,y,z) ) = "sub "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Addi    (x,y,z) ) = "addi "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Addu    (x,y,z) ) = "addu "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Subu    (x,y,z) ) = "subu "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Addiu   (x,y,z) ) = "addiu "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Mul     (x,y,z) ) = "mul "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Mult    (x,y) )   = "mult "^prettyRegs x^","^prettyRegs y
  | pretty (Div     (x,y) )   = "div "^prettyRegs x^","^prettyRegs y
  | pretty (And     (x,y,z) ) = "and "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Or      (x,y,z) ) = "or "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Andi    (x,y,z) ) = "andi "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Ori     (x,y,z) ) = "ori "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Sll     (x,y,z) ) = "sll "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Srl     (x,y,z) ) = "srl "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Load    (x,y,z) ) = "load "^prettyRegs x^","^Int.toString y^","^prettyRegs z
  | pretty (Store   (x,y,z) ) = "store "^prettyRegs x^","^Int.toString y^","^prettyRegs z
  | pretty (Lui     (x,y) )   = "lui "^prettyRegs x^","^Int.toString y
  | pretty (La      (x,y) )   = "la "^prettyRegs x^","^y
  | pretty (Li      (x,y) )   = "li "^prettyRegs x^","^Int.toString y
  | pretty (Mfhi    x )       = "mfhi "^prettyRegs x
  | pretty (Mflo    x )       = "mflo "^prettyRegs x
  | pretty (Move    (x,y) )   = "move "^prettyRegs x^","^prettyRegs y
  | pretty (Beq     (x,y,z) ) = "beq "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Bne     (x,y,z) ) = "bne "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Bgt     (x,y,z) ) = "bgt "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Bge     (x,y,z) ) = "bge "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Blt     (x,y,z) ) = "blt "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Ble     (x,y,z) ) = "ble "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (Slt     (x,y,z) ) = "slt "^prettyRegs x^","^prettyRegs y^","^prettyRegs z
  | pretty (Slti    (x,y,z) ) = "slti "^prettyRegs x^","^prettyRegs y^","^Int.toString z
  | pretty (J       x )       = "j "^Int.toString x
  | pretty (Jr      x )       = "jr "^prettyRegs x
  | pretty (Jal     x )       = "jal "^Int.toString x
  | pretty (Syscall )         = "syscall "

end

val test = MIPS.Add (MIPS.T0,MIPS.T1,MIPS.T2)
val output = MIPS.pretty test

