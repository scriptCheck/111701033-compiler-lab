signature TEMP = sig
  type temp  (* temporary variables of your program *)
  type label (* temporary labels for your program *)
  val newlabel : unit -> label
  val newtemp  : unit -> temp
  val printTemp : temp -> string
  val printLabel : label -> string
end

structure Temp : TEMP = struct
  type temp = int
  type label = int
  val tempCount = ref 0
  val labelCount = ref 0
  val newTemp () = (!tempCount = !tempCount + 1; !tempCount)
  val newLabel () = (!labelCount = !labelCount + 1; !labelCount)
  val printTemp = Int.toString
  val printLabel = Int.toString
end
