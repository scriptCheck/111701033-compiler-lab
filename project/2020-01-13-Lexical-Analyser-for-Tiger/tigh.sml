structure TIGH =
struct


fun load lexer = case lexer () of
		     SOME i => i :: load lexer
		   | NONE   => []

(*

Lexer suitable for interactive usage. No buffering and hence
slow. However, instant feed back is available and hence should be used
on interactive sessions

*)

val interactive = TigerLex.makeLexer (fn _ => TextIO.inputN (TextIO.stdIn,1))

fun lexfile file = let val strm = TextIO.openIn file
		   in TigerLex.makeLexer (fn n => TextIO.inputN(strm,n))
		   end

(* Running with a lexer *)
fun runWithLexer lexer = let fun loop () = case lexer () of
					       NONE      => ()
					    |  SOME inst => loop (Tiger.step inst)
			 in loop ()
			 end


val _ =  ( case CommandLine.arguments() of
	       [] => runWithLexer interactive
	    |  xs => (List.map (runWithLexer o lexfile) xs; ())
	 )

end
