type lexresult                   = Tokens.Token option
fun eof ()                       = NONE

%%
%structure TigerLex
ws=[\ \r\t];
d=[0-9];
keywords="while"|"for"|"do"|"of"|"nil"|"for"|"to"|"break"|"let"|"in"|"end"|"function"|"var"|"type"|"array"|"if"|"then"|"else";
punctuations=[-(){};+=<>"':/*\,];
%%
{ws}+                            => (SOME   (Tokens.TAB           )     );
\n                               => (SOME   (Tokens.NEWLINE       )     );
"/*".*"*/"\n                     => (SOME   (Tokens.NEWLINE       )     );
{keywords}                       => (SOME   (Tokens.Keyword yytext)     );
[-+]?{d}([.]{d})?([eE][-+]?{d})? => (SOME   (Tokens.Num     yytext)     );
[a-zA-Z_][a-zA-Z0-9_]*           => (SOME   (Tokens.ID      yytext)     );
{punctuations}                   => (SOME   (Tokens.Punct   yytext)     );
.                                => (SOME   (Tokens.Rest    yytext)     );
