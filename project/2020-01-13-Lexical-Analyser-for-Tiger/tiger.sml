(** * The tiger token printing machine? *)

structure Tiger =
struct

(*
datatype Token
  = Keyword of string (* For Keywords	                   *)
  | Punct of string   (* Takes care of punctuation symbols *)
  | Num of string     (* Takes care of Integers            *)
  | ID of string      (* Variables / Identifiers           *)
  | Rest of string    (* Takes care of everything else     *)
*)

(* This function performs a single coloring *)

(* step : Tokens.Token -> unit *)
fun step (Tokens.NEWLINE  )      = Term.printToTerm "\n" Term.OColor
  | step (Tokens.TAB      )      = Term.printToTerm  " " Term.OColor
  | step (Tokens.Keyword x)      = Term.printToTerm   x  Term.KColor
  | step (Tokens.Punct   y)      = Term.printToTerm   y  Term.PColor
  | step (Tokens.Num     z)      = Term.printToTerm   z  Term.NColor
  | step (Tokens.ID      v)      = Term.printToTerm   v  Term.VColor
  | step (Tokens.Rest    w)      = Term.printToTerm   w  Term.OColor

end
