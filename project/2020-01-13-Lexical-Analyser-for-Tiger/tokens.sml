(* The Tokens file *)

structure Tokens =
struct

datatype Token
  = NEWLINE
  | TAB
  | Keyword of string (* For Keywords	                   *)
  | Punct   of string (* Takes care of punctuation symbols *)
  | Num     of string (* Takes care of Integers            *)
  | ID      of string (* Variables / Identifiers           *)
  | Rest    of string (* Takes care of everything else     *)

type Program = Token list

end
