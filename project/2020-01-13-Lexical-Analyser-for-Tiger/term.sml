(** * The terminal file *)

structure Term =
struct

(*
datatype Token
  = Keyword of string (* For Keywords	                   *)
  | Punct of string   (* Takes care of punctuation symbols *)
  | Num of string     (* Takes care of Integers            *)
  | ID of string      (* Variables / Identifiers           *)
  | Rest of string    (* Takes care of everything else     *)
*)

(* Coloring Datatype *)
datatype Color
  = KColor             (* Keywords    *)
  | PColor             (* punctuation *)
  | NColor             (* numbers     *)
  | VColor             (* identifiers *)
  | OColor             (* otherwise   *)

(* Some helper functions for printing the tokens *)

(* hex     : <Esc>[38;5;'ColorNumber'm
*  #268bd2 : \e[38;5;32m   Variable
*  #859900 :         220m  Operator
*  #93a1a1 :         248m  Comment
*  #2aa198 :         37m   DecVal
*  #859900 :         100m  keyword
*  #657b83 :         66m   normal/ordinary
*
* *)

val kcolor  = "\u001b[38;5;100m"
val pcolor  = "\u001b[38;5;220m"
val ncolor  = "\u001b[38;5;37m"
val vcolor  = "\u001b[38;5;32m"
val ocolor  = "\u001b[38;5;66m"
val normal  = "\u001b[0m"

(* printTok : String -> Color -> Unit *)
fun printToTerm x KColor = print ( kcolor^x^normal)
  | printToTerm x PColor = print ( pcolor^x^normal)
  | printToTerm x NColor = print ( ncolor^x^normal)
  | printToTerm x VColor = print ( vcolor^x^normal)
  | printToTerm x OColor = print ( ocolor^x^normal)

end
